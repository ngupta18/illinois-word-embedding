package edu.illinois.cs.cogcomp.nlp.wordmap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class EfficientWordMap {
	private HashMap<String, Long> map = new HashMap<String, Long>();
	private HashMap<String,double[]> cache = new HashMap<String,double[]>();
	private BufferedRaf raf = null;
	private int dim;
	private boolean useCache;

	public String UNKNOWN = "UUUNKKK";

	/*
	 * dict: word embedding file (must be in space separated form: <word> <idx 1> <idx 2> ...
	 * dim: dimension of word embeddings
	 * useCache: if true, cache vectors that have been seen. This increases memory but 
	 * provides some speed-up for common words.
	 */
	public EfficientWordMap(String dict, int dim) throws FileNotFoundException {
		this(dict,dim,false);
	}

	/*
	 * dict: word embedding file (must be in space separated form: <word> <idx 1> <idx 2> ...
	 * dim: dimension of word embeddings
	 * useCache: if true, cache vectors that have been seen. This increases memory but 
	 * provides some speed-up for common words.
	 */
	public EfficientWordMap(String dict, int dim, boolean useCache) throws FileNotFoundException {

		long t1 = System.currentTimeMillis();
		this.dim = dim;

		try {
			File f = new File(dict);
			raf = new BufferedRaf(f,"r");
			long n = raf.getFilePointer();
			String l = raf.readLine();
			while(l != null ) {

				String[] toks = l.split("\\s");
				String wd = toks[0];

				wd = wd.trim();

				map.put(wd, n);
				n = raf.getFilePointer();
				l = raf.readLine2();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		long t2 = System.currentTimeMillis();
		//System.out.println("Time to load embeddings: "+((t2-t1)/1000.)+" seconds");
	}

	/*
	 * gets word vector for given word. Does soft matching in that
	 * it first checks for word exactly, then checks for word lower-cased,
	 * and finally will return UUUNNKK vector.
	 */
	public synchronized double[] lookup(String w1) {

		if(cache.containsKey(w1))
			return cache.get(w1);

		if(!map.containsKey(w1)) {
			w1 = w1.toLowerCase();
			if(!map.containsKey(w1)) {
				w1 = UNKNOWN;
			}
		}

		long l = 0;
		if(map.containsKey(w1))
			l = map.get(w1);
		else
			return null;
		
		String s = "";
		try {
			raf.seek(l);
			s=raf.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if(s.equals(""))
			return null;

		s = s.trim();
		if( s.length() == 0)
			return null;

		String[] toks = s.split("\\s");

		double[] vector = new double[dim];
		for(int i = 1; i < toks.length; i++) {
			String t = toks[i];
			vector[i-1] = Double.parseDouble(t);
		}	

		if(useCache)
			cache.put(w1, vector);

		return vector;
	}

	public boolean isUnknown(String w1) {
		if(!map.containsKey(w1)) {
			w1 = w1.toLowerCase();
			if(!map.containsKey(w1)) {
				return true;
			}
		}
		
		return false;
	}

	/*
	 * Returns cosine similarity of w1 and w2. If both
	 * words are outside the vocabulary and the words
	 * are not equal, returns 0.
	 */
	public double cosineSim(String w1, String w2) {
		if(w1.equals(w2))
			return 1.;
		
		boolean unk_1 = isUnknown(w1);
		boolean unk_2 = isUnknown(w2);
		
		if(unk_1 && unk_2)
			return 0.;
		
		double[] v1 = lookup(w1);
		double[] v2 = lookup(w2);
		
		if(v1 == null || v2 == null)
			return 0.;
		
		return cosine(v1,v2);			
	}

	private double cosine(double[] vec1, double[] vec2) {
		double cosine = 0;
		double t1 = 0;
		double t2 = 0;

		for(int i=0; i < vec1.length; i++) {
			cosine += vec1[i]*vec2[i];
			t1 += vec1[i]*vec1[i];
			t2 += vec2[i]*vec2[i];
		}

		return cosine / (Math.sqrt(t1)*Math.sqrt(t2));
	}
}
